FROM php:7.4-apache AS dev

RUN apt-get update
RUN apt-get install -y vim
RUN echo "alias ll='ls -lha'" >> /root/.bashrc
RUN apt-get install -y git

# Apache
RUN a2enmod rewrite
RUN a2enmod ssl
RUN a2enmod headers

COPY ./apache.crt /etc/ssl/certs/
COPY ./apache.key /etc/ssl/private/
COPY ./000-default.conf /etc/apache2/sites-enabled/
COPY ./000-default.ssl.conf /etc/apache2/sites-enabled/
RUN mkdir -p /srv/www/default/html
RUN mkdir -p /srv/www/default/log
RUN mkdir -p /srv/www/booking/html
RUN mkdir -p /srv/www/booking/html

# PHP
COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/bin/
RUN install-php-extensions mysqli
RUN install-php-extensions gettext
RUN install-php-extensions gd
RUN install-php-extensions redis
RUN install-php-extensions solr
RUN install-php-extensions zip
RUN install-php-extensions bcmath
RUN install-php-extensions sockets
RUN install-php-extensions amqp
RUN install-php-extensions mcrypt
RUN install-php-extensions pdo_mysql

RUN cp /usr/local/etc/php/php.ini-development /usr/local/etc/php/php.ini

RUN service apache2 restart

EXPOSE 80
EXPOSE 443